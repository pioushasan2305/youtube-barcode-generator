#!/usr/bin/env python


# import required libraries
import argparse
import os
import string
import numpy as np
from src.bar import Moviebarcode
import pathlib


def vid2barcode(vidurl,title):

    moviebarcode = Moviebarcode(vidurl)
    moviebarcode.generate()
    pathname = os.path.join(os.getcwd(), "static", "imgnjson")
    #filename = os.path.split(video_path)[-1].split('.')[0]
    filename=format_filename(title)
    # Create an image, .png file
    image_filename = os.path.join(pathname,filename+'.png')
    moviebarcode.make_image(file_name=image_filename)
    # write to json file
    json_filename = os.path.join(pathname,filename+'.json')
    moviebarcode.write2json(json_filename)


def format_filename(s):
    valid_chars = "-_.() %s%s" % (string.ascii_letters, string.digits)
    filename = ''.join(c for c in s if c in valid_chars)
    filename = filename.replace(' ', '_')
    return filename

