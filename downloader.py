
import youtube_dl
from interm import vid2barcode

def download(url):
    video_url=url
    ydl_opts = {
        'format': 'worst',
    }
    ydl = youtube_dl.YoutubeDL(ydl_opts)
    info_dict = ydl.extract_info(video_url, download=False)
    title = info_dict.get('title', None)
    url = info_dict.get('url', None)
    vid2barcode(url, title)

def downloadplaylist(urls):
    ydl = youtube_dl.YoutubeDL({'outtmpl': '%(id)s%(ext)s', 'quiet': True, })
    video = ""

    with ydl:
        result = ydl.extract_info \
            (urls,
             download=False)  # We just want to extract the info

        if 'entries' in result:
            # Can be a playlist or a list of videos
            video = result['entries']

            # loops entries to grab each video_url
            for i, item in enumerate(video):
                video = result['entries'][i]['webpage_url']
                download(video)