from flask import Flask,request,render_template,make_response
from downloader import download,downloadplaylist
from deleter import delete
from json import dumps
from flask_cors import CORS
import os
app = Flask(__name__)
CORS(app)
app.config['SECRET_KEY']='qasdewr'



@app.route('/', methods=['POST', 'GET'])
def login():
    return render_template("base.html")
@app.route('/send', methods=['POST', 'GET'])
def jsr():
    if request.method == 'GET':
        link = list(request.args.values())[0]
        return link
    else:
        link = list(request.args.values())[0]
        download(link)
        return link
@app.route('/del', methods=['POST', 'GET'])
def delet():
    if request.method == 'GET':
        delete()
        return 'deleted'
    else:
        delete()
        return 'deleted'
@app.route('/sendlist', methods=['POST', 'GET'])
def jsrlist():
    if request.method == 'GET':
        link = list(request.args.values())[0]
        return link
    else:
        link = list(request.args.values())[0]
        downloadplaylist(link)
        return link
@app.route('/imglist', methods=['POST', 'GET'])
def random_image():
    names = os.listdir(os.path.join(app.static_folder, 'imgnjson'))
    return make_response(dumps(names))

if __name__=='__main__':
    app.run(debug=True,port=8000)